package ut.com.riadalabs.jira.plugins.insight.report.payroll;

import com.google.common.collect.Lists;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade;
import com.riadalabs.jira.plugins.insight.common.exception.InsightException;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportParameters;
import com.riadalabs.jira.plugins.insight.reports.payroll.Value;
import com.riadalabs.jira.plugins.insight.reports.payroll.builder.IQLBuilder;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeBean;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IQLBuilderTest {

    private final static Integer schemaId = 10;
    private final static Integer objectTypeId = 20;

    private final static Integer canadianSalaryId = 30;
    private final static String canadianSalaryName = "SALARY(CAD)";

    private final static Integer startDateId = 50;
    private final static Integer endDateId = 60;

    private final static String startDateName = "START_DATE";
    private final static String endDateName = "END_DATE";

    private Value numberTypeAttribute;
    private List<Value> dateTypeAttributes;

    private ObjectTypeAttributeFacade objectTypeAttributeFacadeMock = mock(ObjectTypeAttributeFacade.class);

    @Before
    public void setUp() {
        numberTypeAttribute = new Value(canadianSalaryId);
        dateTypeAttributes = Lists.newArrayList(new Value(startDateId), new Value(endDateId));
    }

    @Test
    public void should_build_correct_object_type_in() {
        final ObjectTypeBean objectTypeBean = new ObjectTypeBean(objectTypeId);

        final IQLBuilder iqlBuilder = new IQLBuilder(objectTypeAttributeFacadeMock);

        final String result = iqlBuilder.addObjectType(objectTypeBean).build();

        final String expected = "objectTypeId IN (20)";

        assertThat(result).as("Correct IQL Object Type")
                .isEqualTo(expected);
    }


    @Test
    public void should_build_correct_date_string_as_exclusive_between() throws InsightException {

        final LocalDateTime customStartDate = LocalDate.of(2019, 1, 1)
                .atStartOfDay();

        final LocalDateTime customEndDate = LocalDate.of(2019, 1, 31)
                .atStartOfDay();

        final PayrollReportParameters payrollReportParameters =
                PayrollHelperFactory.createBasicReportWithinCustomPeriod(schemaId, objectTypeId, canadianSalaryId,
                        startDateId, endDateId, customStartDate, customEndDate);

        when(objectTypeAttributeFacadeMock.loadObjectTypeAttributeBean(anyInt())).thenReturn(
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE),
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE));

        final IQLBuilder iqlBuilder = new IQLBuilder(objectTypeAttributeFacadeMock);

        final String result = iqlBuilder.addDateAttributes(dateTypeAttributes, payrollReportParameters)
                .build();

        final String expectedFirstBetween =
                "(\"START_DATE\" >= \"2019-01-01 00:00\" AND " + "\"START_DATE\" < \"2019-02-01 00:00\")";

        final String expectedSecondBetween =
                "(\"END_DATE\" >= \"2019-01-01 00:00\" AND " + "\"END_DATE\" < \"2019-02-01 00:00\")";

        assertThat(result).as("Correct IQL Date String")
                .isEqualTo(String.join(" OR ", expectedFirstBetween, expectedSecondBetween));
    }

    @Test
    public void should_build_correct_attribute_string() throws InsightException {

        final PayrollReportParameters payrollReportParameters =
                PayrollHelperFactory.createBasicReportWithinLastMonth(schemaId, objectTypeId, canadianSalaryId,
                        startDateId, endDateId);

        when(objectTypeAttributeFacadeMock.loadObjectTypeAttributeBean(anyInt())).thenReturn(
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.DOUBLE));

        final IQLBuilder iqlBuilder = new IQLBuilder(objectTypeAttributeFacadeMock);

        final String result = iqlBuilder.addNumericAttributes(Lists.newArrayList(payrollReportParameters.getNumAttribute()))
                .build();

        final String expected = "(\"SALARY(CAD)\" IS NOT EMPTY)";

        assertThat(result).as("Correct IQL Number String")
                .isEqualTo(expected);
    }

    @Test
    public void should_build_correct_custom_query() throws InsightException {
        final ObjectTypeBean employeeType =
                PayrollHelperFactory.createObjectType(schemaId, objectTypeId);
        final String customQuery = "name like \"John Smith\"";

        final IQLBuilder iqlBuilder = new IQLBuilder(objectTypeAttributeFacadeMock);

        final String result = iqlBuilder.addObjectType(employeeType)
                .addCustomIQL(customQuery)
                .build();

        final String expected = "objectTypeId IN (20) AND name like \"John Smith\"";

        assertThat(result).as("Correct IQL Custom Query")
                .isEqualTo(expected);
    }

}
