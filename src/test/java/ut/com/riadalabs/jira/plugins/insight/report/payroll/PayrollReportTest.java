package ut.com.riadalabs.jira.plugins.insight.report.payroll;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectSchemaFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeFacade;
import com.riadalabs.jira.plugins.insight.reports.payroll.Constants;
import com.riadalabs.jira.plugins.insight.reports.payroll.Expenditure;
import com.riadalabs.jira.plugins.insight.reports.payroll.ExpenditureType;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReport;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportData;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportParameters;
import com.riadalabs.jira.plugins.insight.reports.payroll.builder.IQLBuilder;
import com.riadalabs.jira.plugins.insight.services.model.ObjectBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectSchemaBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeBean;
import com.riadalabs.jira.plugins.insight.services.progress.ProgressCategory;
import com.riadalabs.jira.plugins.insight.services.progress.model.ProgressId;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PayrollReportTest {

    private final static Integer schemaId = 10;
    private final static ObjectSchemaBean validSchema = new ObjectSchemaBean(schemaId);

    private final static Integer objectTypeId = 20;
    private final static ObjectTypeBean validObjectType = new ObjectTypeBean(objectTypeId);

    private final static Integer canadianSalaryId = 30;
    private final static Double entrySalary = 10000.00;

    private final static String canadianSalaryName = "SALARY(CAD)";

    private final static Integer startDateId = 50;
    private final static Integer endDateId = 60;

    private final static String startDateName = "START_DATE";
    private final static String endDateName = "END_DATE";

    private final static LocalDate companyCreationDate = LocalDate.of(2019, 1, 1);

    private ObjectSchemaFacade objectSchemaFacadeMock = mock(ObjectSchemaFacade.class);
    private ObjectTypeFacade objectTypeFacade = mock(ObjectTypeFacade.class);
    private ObjectTypeAttributeFacade objectTypeAttributeFacadeMock = mock(ObjectTypeAttributeFacade.class);
    private ApplicationProperties applicationPropertiesMock = mock(ApplicationProperties.class);

    private final PayrollReport report =
            new PayrollReport(objectSchemaFacadeMock, objectTypeFacade, objectTypeAttributeFacadeMock,
                    applicationPropertiesMock);

    @Test
    public void should_generate_an_empty_report() {
        // 5 day reports
        final LocalDateTime reportStart = companyCreationDate.atStartOfDay();
        final LocalDateTime reportEnd = companyCreationDate.plusDays(4)
                .atStartOfDay();

        final PayrollReportParameters payrollReportParameters =
                PayrollHelperFactory.createBasicReportWithinCustomPeriod(schemaId, objectTypeId,
                        canadianSalaryId, startDateId, endDateId, reportStart, reportEnd);

        final ProgressId progressId = new ProgressId("payroll-test", ProgressCategory.GENERATE_DATA);

        final PayrollReportData generatedPayrollReportData =
                report.generate(payrollReportParameters, Lists.newArrayList(), progressId);

        assertThat(generatedPayrollReportData.hasData()).as("Generated Report Has No Data")
                .isFalse();
    }

    @Test
    public void should_generate_a_report() throws Exception {

        // 6 Hires and 2 Departures in 5 days
        final List<ObjectBean> objects = Lists.newArrayList(createEmployee(1, companyCreationDate, null),
                createEmployee(2, companyCreationDate, companyCreationDate.plusDays(3)),
                createEmployee(3, companyCreationDate, companyCreationDate.plusDays(4)),
                createEmployee(4, companyCreationDate.plusDays(1), null),
                createEmployee(5, companyCreationDate.plusDays(2), null),
                createEmployee(6, companyCreationDate.plusDays(3), null));

        // 5 day reports
        final LocalDateTime reportStart = companyCreationDate.atStartOfDay();
        final LocalDateTime reportEnd = companyCreationDate.plusDays(4)
                .atStartOfDay();

        final PayrollReportParameters payrollReportParameters =
                PayrollHelperFactory.createBasicReportWithinCustomPeriod(schemaId, objectTypeId,
                        canadianSalaryId, startDateId, endDateId, reportStart, reportEnd);

        final ProgressId progressId = new ProgressId("payroll-test", ProgressCategory.GENERATE_DATA);

        validate(payrollReportParameters);
        buildIql(payrollReportParameters);

        when(objectTypeAttributeFacadeMock.loadObjectTypeAttributeBean(anyInt())).thenReturn(
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.INTEGER),
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE),
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE));

        when(applicationPropertiesMock.getOption(APKeys.JIRA_DATE_TIME_PICKER_USE_ISO8601)).thenReturn(false);

        final PayrollReportData generatedPayrollReportData =
                report.generate(payrollReportParameters, objects, progressId);

        final Map<LocalDate, List<Expenditure>> expendituresByDay = generatedPayrollReportData.getExpendituresByDay();

        assertThat(expendituresByDay).as("Generated Report Has Data")
                .hasSize(5);

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 0), canadianSalaryName,
                3 * entrySalary, 0.0)).as("Expenditure Day 1")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 1), canadianSalaryName,
                1 * entrySalary, 0.0)).as("Expenditure Day 2")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 2), canadianSalaryName,
                1 * entrySalary, 0.0)).as("Expenditure Day 3")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 3), canadianSalaryName,
                1 * entrySalary, 1 * entrySalary)).as("Expenditure Day 4")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 4), canadianSalaryName,
                0.0,1.0 * entrySalary)).as("Expenditure Day 5")
                .isTrue();
    }

    @Test
    public void should_respect_date_ordering_report() throws Exception {

        // 6 Hires and 2 Departures in 5 days
        final List<ObjectBean> objects = Lists.newArrayList(createEmployee(1, companyCreationDate, null),
                createEmployee(2, companyCreationDate, companyCreationDate.plusDays(3)),
                createEmployee(3, companyCreationDate, companyCreationDate.plusDays(4)),
                createEmployee(4, companyCreationDate.plusDays(1), null),
                createEmployee(5, companyCreationDate.plusDays(2), null),
                createEmployee(6, companyCreationDate.plusDays(3), null));

        // 5 day report
        final LocalDateTime reportStart = companyCreationDate.atStartOfDay();
        final LocalDateTime reportEnd = companyCreationDate.plusDays(4)
                .atStartOfDay();

        // START DATE AND END DATE HAVE BEEN SWITCHED AROUND
        final PayrollReportParameters payrollReportParameters =
                PayrollHelperFactory.createBasicReportWithinCustomPeriod(schemaId, objectTypeId,
                        canadianSalaryId, endDateId, startDateId, reportStart, reportEnd);

        final ProgressId progressId = new ProgressId("payroll-test", ProgressCategory.GENERATE_DATA);

        validate(payrollReportParameters);
        buildIql(payrollReportParameters);

        when(objectTypeAttributeFacadeMock.loadObjectTypeAttributeBean(anyInt())).thenReturn(
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.INTEGER),
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE),
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE));

        when(applicationPropertiesMock.getOption(APKeys.JIRA_DATE_TIME_PICKER_USE_ISO8601)).thenReturn(false);

        final PayrollReportData generatedPayrollReportData =
                report.generate(payrollReportParameters, objects, progressId);

        final Map<LocalDate, List<Expenditure>> expendituresByDay = generatedPayrollReportData.getExpendituresByDay();

        assertThat(expendituresByDay).as("Generated Report Has Data")
                .hasSize(5);

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 0), canadianSalaryName,
                0.0, 3 * entrySalary)).as("Expenditure Day 1")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 1), canadianSalaryName,
                0.0, 1 * entrySalary)).as("Expenditure Day 2")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 2), canadianSalaryName,
                0.0, 1 * entrySalary)).as("Expenditure Day 3")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 3), canadianSalaryName,
                1 * entrySalary, 1 * entrySalary)).as("Expenditure Day 4")
                .isTrue();

        assertThat(expectedExpenditures(expenditureAtDay(expendituresByDay, 4), canadianSalaryName,
                1.0 * entrySalary, 0.0)).as("Expenditure Day 5")
                .isTrue();
    }

    private void validate(final PayrollReportParameters payrollReportParameters) throws Exception {

        final ObjectTypeAttributeBean numberType =
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.DOUBLE);

        final ObjectTypeAttributeBean startDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        final ObjectTypeAttributeBean endDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        when(objectSchemaFacadeMock.loadObjectSchemaBean(schemaId))
                .thenReturn(validSchema);

        when(objectTypeAttributeFacadeMock.findObjectTypeAttributeBeans(objectTypeId))
                .thenReturn(Lists.newArrayList(numberType, startDateType, endDateType));

        report.validate(payrollReportParameters);
    }

    private void buildIql(final PayrollReportParameters payrollReportParameters) throws Exception {

        when(objectTypeFacade.loadObjectTypeBean(objectTypeId))
                .thenReturn(validObjectType);

        when(objectTypeAttributeFacadeMock.loadObjectTypeAttributeBean(anyInt())).thenReturn(
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE),
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE),
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.DOUBLE));

        final String completeIQL = report.buildIQL(payrollReportParameters);

        final String startDate = payrollReportParameters.determineStartDate(false)
                .atStartOfDay().format(Constants.IQL_DATE_FORMATTER);

        final String endDate = payrollReportParameters.determineEndDate(false)
                .atStartOfDay().plusDays(1).format(Constants.IQL_DATE_FORMATTER);

        assertThat(completeIQL).isEqualTo(
                "objectTypeId IN (20)" +
                " AND " +
                "(\"START_DATE\" >= \"" + startDate + "\" AND " + "\"START_DATE\" < \"" + endDate + "\")" +
                " OR " +
                "(\"END_DATE\" >= \"" + startDate + "\" AND " + "\"END_DATE\" < \"" + endDate + "\")" +
                " AND " +
                "(\"SALARY(CAD)\" IS NOT EMPTY)");

    }

    private Expenditure expenditureAtDay(final Map<LocalDate, List<Expenditure>> expenditureByDay, int day) {
        return expenditureByDay.get(companyCreationDate.atStartOfDay()
                .toLocalDate()
                .plusDays(day))
                .get(0);
    }

    private boolean expectedExpenditures(final Expenditure expenditure, final String name, final Double in,
            final Double out) {

        return name.equals(expenditure.getName()) && in.equals(expenditure.getTypeValueMap()
                .get(ExpenditureType.IN)) && out.equals(expenditure.getTypeValueMap()
                .get(ExpenditureType.OUT));
    }

    private ObjectBean createEmployee(final Integer employeeId, final LocalDate startDate, final LocalDate endDate) {

        final Map<Integer, Double> salaryMap = Maps.newHashMap();
        salaryMap.put(canadianSalaryId, entrySalary);

        final Map<Integer, LocalDate> employmentHistoryMap = Maps.newHashMap();
        employmentHistoryMap.put(startDateId, startDate);
        employmentHistoryMap.put(endDateId, endDate);

        return EmployeeFactory.createEmployeeBean(employeeId, objectTypeId, salaryMap, employmentHistoryMap);
    }

}
