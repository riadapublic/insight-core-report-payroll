package com.riadalabs.jira.plugins.insight.reports.payroll.builder;

import com.google.common.base.Strings;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade;
import com.riadalabs.jira.plugins.insight.reports.payroll.Constants;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportParameters;
import com.riadalabs.jira.plugins.insight.reports.payroll.Value;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeBean;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class IQLBuilder {

    private static final String AND = " AND ";
    private static final String OR = " OR ";
    private static final String IN = " IN ";
    private static final String BETWEEN = "\"%s\" >= \"%s\" AND \"%s\" < \"%s\"";
    private static final String NONEMPTY = "\"%s\" IS NOT EMPTY";

    private final List<String> parameters;
    private final ObjectTypeAttributeFacade objectTypeAttributeFacade;

    public IQLBuilder(final ObjectTypeAttributeFacade objectTypeAttributeFacade) {
        this.parameters = new ArrayList<>();
        this.objectTypeAttributeFacade = objectTypeAttributeFacade;
    }

    public IQLBuilder addObjectType(final ObjectTypeBean objectTypeBean) {
        // TODO not sure how this is generated and client wont either
        if (objectTypeBean.isInherited()) {
            parameters.add(addForHierarchy(objectTypeBean));
        } else {
            parameters.add(addForSingle(objectTypeBean));
        }

        return this;
    }

    private String addForSingle(final ObjectTypeBean objectTypeBean) {
        return "objectTypeId"
                .concat(IN)
                .concat(parenthesis(objectTypeBean.getId().toString()));
    }

    private String addForHierarchy(final ObjectTypeBean objectTypeBean) {
        return "objectTypeId"
                .concat(IN)
                .concat(parenthesis(objectTypeBean.getId().toString(),
                        objectTypeBean.getParentObjectTypeId().toString()));
    }

    public IQLBuilder addDateAttributes(final List<Value> dateAttributes, final PayrollReportParameters payrollReportParameters) {

        final boolean doesWeekBeginOnMonday = true;

        final String iqlStart = payrollReportParameters.determineStartDate(doesWeekBeginOnMonday)
                .format(Constants.IQL_DATE_FORMATTER);
        // exclusive between
        final String iqlEnd = payrollReportParameters.determineEndDate(doesWeekBeginOnMonday)
                .plusDays(1)
                .atStartOfDay()
                .format(Constants.IQL_DATE_FORMATTER);

        List<String> dateAttributeNames = dateAttributes.stream()
                .map(dateAttribute -> getAttibuteBeanName(dateAttribute.getValue(), objectTypeAttributeFacade))
                .collect(Collectors.toList());

        final String betweenStatements = dateAttributeNames.stream()
                .map(dateAttributeName -> parenthesis(String.format(BETWEEN,
                        dateAttributeName, iqlStart, dateAttributeName, iqlEnd)))
                .collect(Collectors.joining(OR));

        this.parameters.add(betweenStatements);

        return this;
    }

    public IQLBuilder addNumericAttributes(final List<Value> numAttributes) {
        final String orString = numAttributes.stream()
                .map(numAttribute -> getAttibuteBeanName(numAttribute.getValue(), objectTypeAttributeFacade))
                .map(attributeName -> String.format(NONEMPTY, attributeName))
                .collect(Collectors.joining(OR));

        this.parameters.add(parenthesis(orString));

        return this;
    }

    public IQLBuilder addCustomIQL(final String iqlString) {
        if (!Strings.isNullOrEmpty(iqlString)) {
            parameters.add(iqlString);
        }

        return this;
    }

    public String build() {
        return String.join(AND, parameters);
    }

    private String getAttibuteBeanName(final Integer id, final ObjectTypeAttributeFacade objectTypeAttributeFacade) {
        try {
            return objectTypeAttributeFacade.loadObjectTypeAttributeBean(id)
                    .getName();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String parenthesis(final String... input) {
        return "(" + String.join(", ", input) + ")";
    }
}
