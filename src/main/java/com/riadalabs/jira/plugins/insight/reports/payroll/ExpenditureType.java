package com.riadalabs.jira.plugins.insight.reports.payroll;

public enum ExpenditureType {
    IN,
    OUT
}
