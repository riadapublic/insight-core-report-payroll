# Insight Report - Payroll
How to create a custom report using Riada's Widget Framework.  
This guide is geared towards developers.

![Payroll Example Chart](images/Payroll-example.png)  
 
## Create a Jira Plugin

Set up a basic plugin following Atlassian's [guide](https://developer.atlassian.com/server/framework/atlassian-sdk/create-a-helloworld-plugin-project/)  
Following the example will generate a simple pom; see this [pom](pom.xml) for a complete view on what dependencies/plugins are required for this report.

##Widget Framework

#### Overview

The widget framework consists of 3 parts:
* Widget Parameters (input)
* Widget Data (output)
* Widget Module (engine)

All are exposed as interfaces by Insight and will need to be implemented to create a successful report.

#### Widget Parameters

The widget parameters represent the parameters(fields) that will be used to generate the report.

![Parameters](images/Parameters-example.png)

To achieve this implement the [WidgetParameters class](src/main/java/com/riadalabs/jira/plugins/insight/reports/payroll/PayrollReportParameters.java)
and implement the insight-widget module type parameters element in the [plugin descriptor](src/main/resources/atlassian-plugin.xml) (to make it appear on screen)

#### Widget Data

The widget data represents the form in which the report will be consumed by the front-end renderers.

To achieve this implement the [WidgetData class](src/main/java/com/riadalabs/jira/plugins/insight/reports/payroll/PayrollReportData.java)   

#### Widget Module 

The widget module will generate the report.

There are 3 places that we allow for customization within the widget framework:
- Validation
- Query building
- Generating the report

To achieve this implement the [WidgetModule and GeneratingDataByIQLCapability classes](src/main/java/com/riadalabs/jira/plugins/insight/reports/payroll/PayrollReport.java)

These are the (currently) exposed components from used to interface with Insight:
- ObjectSchemaFacade
- ObjectTypeFacade
- ObjectTypeAttributeFacade
- ObjectFacade
- ConfigureFacade
- IqlFacade
- ObjectAttributeBeanFactory
- ImportSourceConfigurationFacade
- InsightPermissionFacade
- InsightGroovyFacade
- ProgressFacade

#### Validate

        public void validate(@NotNull WidgetParameters) throws Exception 

Validate that the widget parameters have been created correctly. For instance, in this example we make sure that the object attribute types  
still correspond to the actual object attributes i.e,
>  Employee has a salary attribute that was expected as a numeric type but is now textual

#### Build IQL

    public String buildIQL(@NotNull WidgetParameters parameters) throws Exception 

Build a query from the widget parameters. This query will be used to fetch the objects.  

*TODO offer some kind of builder or easier way to generate a valid query.*

#### Generate

    public WidgetData generate(@NotNull WidgetParameters parameters, List<ObjectBean> objects,
                @NotNull ProgressId progressId) 

Generate the widget data from the returned objects.

The progressId corresponds to the progress of the current report job.
Use the ProgressFacade to extract any pertinent information.   

## Implement the Widget Module in the plugin descriptor

This is the way to register your custom report widget as a plugin within Insight.  
*Please note that all label names will need to be unique.*

##### Module Type

    <insight-widget class="com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReport"
    
Point to the WidgetModule

##### Renderers

    <renderers>
        <renderer mapper="BarChart.Mapper"
                  view="BarChart.View"
                  label="Bar Chart"/>
    </renderers>  
    
These are the names of functions in the js [component(s)](src/main/resources/js/insight-report-payroll.js) that will transform and display the data generated by the backend.  
The graphical display is rendered within an iframe.

The Mapper and View functions need to follow the signature:
    
    Mapper = function (data, parameters, baseUrl) { .. }
    
* data: widget data
* parameters: widget parameters
* baseUrl: ...

- return: transformed data
    
    
    View = function (mappedData){ ... }
    
* mappedData: the output of Mapper

- return: void

In the view function append whatever to the DOM making sure the parent element is:

    <div id="riada" class="js-riada-widget">

Place any resources to be downloaded in a <web-resource> tag in the plugin descriptor.

    <web-resource key="insight-report-payroll" i18n-name-key="Payroll Report Resource">
        <resource type="download" name="insight-report-payroll.css"
                  location="/css/insight-report-payroll.css"/>
        <resource type="download" name="insight-report-payroll.js"
                  location="/js/insight-report-payroll.js"/>
        <context>insight-report-payroll</context>
    </web-resource>

##### Exporters

    <exporters>
       <exporter transformer="Transformer.JSON"
                 extension="json"
                 label="insight.example.report.exporter.json"/>
    </exporters>

Define any exporters for your data. The data exported will not be the WidgetData but the output of the Mapper.

    Transformer.JSON = function (mappedData) { ... }
    
* mappedData: output of Mapper    
    
- return: data transformed to extension type

Exporters are displayed in the created report not the preview ![exporter](images/Exporter-example.png)

##### Parameters

    <parameter key="numAttribute"
               type="objecttypeattributepicker"
               label="insight.example.report.attribute.numeric"
               required="true">
        <configuration>
            <dependency key="objectType"/>
            <filters>
                <value>INTEGER</value>
                <value>DOUBLE</value>
            </filters>
        </configuration>
    </parameter>
    
These are the options displayed in the report parameters form.

 The key will correspond to the widget parameters field name.  
 
 All the type options (currently) are:
 * checkbox
 * datepicker
 * datetimepicker
 * iql
 * jql
 * number
 * objectpicker
 * objectschemapicker
 * objectsearchfilterpicker
 * objecttypeattributepicker
 * objecttypepicker
 * projectpicker
 * radiobutton
 * schemapicker
 * select
 * simpleobjecttypepicker
 * switch
 * text
 * timepicker
 * userpicker
 
 Dependencies are relative to other parameters and filters on what types are returnable.  






